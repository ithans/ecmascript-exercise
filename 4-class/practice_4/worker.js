// TODO 17: 在这里写实现代码
import Person from './person';
class Worker extends Person {
  introduce() {
    let res =
      'My name is ' +
      this.name +
      '. I am ' +
      this.age +
      ' years old. ' +
      'I am a Worker. I have a job.';
    return res;
  }
}
export default Worker;

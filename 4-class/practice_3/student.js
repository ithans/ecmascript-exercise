// TODO 13: 在这里写实现代码
import Person from './Person';
class Student extends Person {
  constructor(name, age, klass) {
    super(name, age);
    this.klass = klass;
  }
  introduce() {
    let res = 'I am a Student. I am at Class ' + this.klass + '.';
    return res;
  }
}
export default Student;

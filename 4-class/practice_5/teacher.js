// TODO 20: 在这里写实现代码
import Student from './student';
class Teacher extends Student {
  constructor(name, age, klass) {
    super(name, age);
    this.klass = klass;
  }
  introduce() {
    let res;
    if (this.klass != undefined) {
      res =
        'My name is ' +
        this.name +
        '. I am ' +
        this.age +
        ' years old. ' +
        'I am a Teacher. I teach Class ' +
        this.klass +
        '.';
    } else {
      res =
        'My name is ' +
        this.name +
        '. I am ' +
        this.age +
        ' years old. ' +
        'I am a Teacher. I teach No Class.';
    }
    return res;
  }
}
export default Teacher;
